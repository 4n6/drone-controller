# Drone Controller Manual

The `controller` binary is a flight plan validation interface for a drone.

The flight plan is the sequence of movements the drone is programmed to
perform. Before each step of this sequence, the program operator must review
the next step and "approve" the command for execution by typing the validation
token that corresponds to the command.

The flight can be interrupted at any time using the `Ctrl + C` sequence.

### Updates:
 - `controller` binary deployed in the program operators VM

### Open Issues: 
 - [See here]

[see here]: <https://gitlab.com/4n6/drone-controller/issues>